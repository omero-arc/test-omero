# Test OMERO instance for the 2nd DenBI Hackathon

Test OMERO instance with preloaded fake data and annotations.

The instance is based on this example: https://github.com/ome/docker-example-omero/tree/master

The `docker-compose.yml` file was modified to use the mount the 2 local folders `docker-example-omero_database` and `docker-example-omero_omero` as volumes, so that OMERO can use the preloaded fake data.

## Running the instance

To run the instance:
```
git clone https://codebase.helmholtz.cloud/omero-arc/test-omero.git
cd test-omero
unzip docker-example-omero_database.zip
unzip docker-example-omero_omero.zip
docker-compose up -d
```
To bring it down:
```
docker-compose down
```
The psql container changes the ownership and permissions of the `./docker-example-omero_database`
folder, you might want to reset them, for copying, dumping the DB, ....

## Test Cases
2 fake test cases:
- Project-Dataset test case
- Screen-Plate test case

### Project-Dataset test case
**Project** -> **TEST_STUDY**
- Dataset -> TEST_DATASET_A
    3 Filestes (1 serie each) -> 3 images   
    
- Dataset -> TEST_DATASET_B
    3 Filesets (4 series each) -> 12 images 


### Screen-Plate test case
**Screen** -> **Scree Name 0**
- Plate -> Plate Name 0 
    - PlateAcquisition ->  PlateAcquisition Name 0 
- Plate -> Plate Name 1
    - PlateAcquisition ->  PlateAcquisition Name 0 

## Fake Data
The fake files used to generate the test cases are in the `test_files` folder.

## To do!
- Add more annotations!
- Add a section on how to add new test cases.
